            .data
tabella:    .word 154, 123, 109, 86, 4, 0, 412, -23, -231, 9, 50, 0, 123, -24, 12, 55, -45, 0, 0, 0, 0, 0, 0, 0
N=3
M=5
            .text
            .globl main
            .ent main
main:       la $s0, tabella
            and $t0, $0, $0
            and $t1, $0, $0
            and $t2, $0, $0
forEsterno: beq $t0, N, exit
            and $t1, $0, $0
            and $t2, $0, $0
forInterno: beq $t1, M, exitInt
            sll $t3, $t0, 2
            sll $t4, $t1, 2
            mul $t3, $t3, M
            add $t3, $t3, $t4
            lw $t5, matr($t5)
            add $t2, $t2, $t5
            addi $t1, $t1, 1
            j forInterno
exitInt:    sll $t3, $t0, 2
            sll $t4, M, 2
            add $t3, $t3, $t4
            sw $t2, matr($t3)
            addi $t0, $t0, 1
            j forEsterno
exit:       li $v0, 10
            syscall
            .end main