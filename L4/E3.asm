            .data
vet1:       .word 1, 2, 3, 4
vet2:       .word 5, 6, 7, 8
matrice:    .space 64
DIM=4
            .text
            .globl main
            .ent main
main:      
            and $t0, $0, $0
            and $t1, $0, $0
            and $t4, $0, $0
            and $t5, $0, $0
forEsterno: beq $t0, DIM, exit
            and $t1, $0, $0
            and $t5, $0, $0
forInterno: beq $t1, DIM, exitInt
            lw $s0, vet1($t4)
            lw $s1, vet2($t5)
            mul $t2, $s0, $s1
            mul $t3, $t4, DIM
            add $t3, $t3, $t5
            sw $t2, matrice($t3)
            addi $t1, $t1, 1
            addi $t5, $t5, 4
            j forInterno
exitInt:    addi $t0, $t0, 1
            addi $t4, $t4, 4
            j forEsterno
exit:
            li $v0, 10
            syscall
            .end main