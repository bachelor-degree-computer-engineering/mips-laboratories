            .data
matrice:    .space 400
DIM=10
            .text
            .globl main
            .ent main
main:      
            addi $t0, $0, 1
            addi $t1, $0, 1
            and $t3, $0, $0
            and $t4, $0, $0
forEsterno: bgt $t0, DIM, exit
            addi $t1, $0, 1
            and $t4, $0, $0
forInterno: bgt $t1, DIM, extInterno
            mul $t2, $t0, $t1
            mul $t5, $t3, DIM
            add $t5, $t5, $t4
            sw $t2, matrice($t5)
            addi $t1, $t1, 1
            addi $t4, $t4, 4
            j forInterno
extInterno: addi $t0, $t0, 1
            addi $t3, $t3, 4
            j forEsterno
exit:       li $v0, 10
            syscall
            .end main