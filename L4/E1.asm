            .data
array:      .word 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
DIM = 20
            .text
            .globl main
            .ent main
            #$t0 = i, $t1 array
main:       addi $t0, $0, 2
            la $t1, array
            li $t2, 1
            sw $t2, ($t1)
            sw $t2, 4($t1)
            add $t1, $t1, 8        
loop:       beq $t0, DIM, exit
            lw $s0, -4($t1)
            lw $s1, -8($t1)
            add $t3, $s0, $s1
            sw $t3, ($t1)
            addi $t1, $t1, 4
            addi $t0, $t0, 1
            j loop
exit:       li $v0, 10
            syscall
            .end main