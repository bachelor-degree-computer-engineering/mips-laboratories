            .data
ENDCHAR = '\n'
            .text
            .globl main
            .ent main

main:       and $t0, $0, $0
readLoop:   li $v0, 12
            syscall
            addi $sp, $sp, -4
            sw $v0, ($sp)
            addi $t0, $t0, 1
            bne $v0, ENDCHAR, readLoop
            addi $sp, $sp, 4
            addi $t0, $t0, -2   #elimino l'ultimo argomento dallo stack
            sll $t0, $t0, 2
            move $t2, $sp       #carico in $t2 ultimo indirizzo 
            add $t1, $sp, $t0   #carico in $t1 primo indirizzo
loop:       blt $t1, $t2, exitS
            lw $t3, ($t1)
            lw $t4, ($t2)
            bne $t3, $t4, exitF
            addi $t2, $t2, 4
            addi $t1, $t1, -4
            j loop
exitS:      li $s0, 1
            j done
exitF:      li $s0, 0 
done:       li $v0, 10
            syscall
            .end main