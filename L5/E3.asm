            .data
msgF:       .asciiz "Non ha soluzioni reali"
msgS:       .asciiz "Ha soluzioni reali"
            .text
            .globl main
            .ent main
main:       #$s0 = a, $s1 = b, $s2 = c
            li $v0, 5
            syscall
            move $s0, $v0
            li $v0, 5
            syscall
            move $s1, $v0
            li $v0, 5
            syscall
            move $s2, $v0
            bne $s0, $0, aIsNot0
            bne $s1, $0, bIsNot0
bIsNot0:    bne $s2, $0, cIsNot0
            j success
cIsNot0:    j failure
aIsNot0:    bne $s2, $0, success
            mul $t0, $s1, $s1
            mul $t1, $s0, $s2
            sll $t1, $t1, 2
            sub $t0, $t0, $t1
            slt $t2, $t0, $0
            beq $t2, $0, failure
            j success
success:    la $a0, msgS
            li $v0, 4
            syscall
            j end
failure:    la $a0, msgF
            li $v0, 4
            syscall
            j end
end:    li $v0, 10
            syscall
            .end main