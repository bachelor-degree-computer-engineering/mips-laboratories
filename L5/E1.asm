            .data
var:        .word  3141592653
ZERO = '0'
DIECI = 10
            .text
            .globl main
            .ent main
main:       lw $s0, var
            add $t0, $0, $0 # $t0 conta quante variabili sono in stack
            li $s1, 10
loop:       divu $s0, $s1
            mflo $s0
            mfhi $t1
            addi $t0, $t0, 1
            addi $sp, $sp, -4
            sw $t1, ($sp)
            bne $s0, 0, loop
printLoop:  beq $t0, $0, done
            lw $a0, ($sp)
            addi $sp, $sp, 4
            add $a0, $a0, ZERO
            li $v0, 11
            syscall
            addi $t0, $t0, -1
            j printLoop
done:
            li $v0, 10
            syscall
            .end main