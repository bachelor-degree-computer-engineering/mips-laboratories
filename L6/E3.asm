            .data
parola:     .ascii "gabriele"
DIM = 8
            .text
            .globl main
            .ent main
main:       la $s0, parola
            and $t0, $0, $0
            li $t1, 8
loop:       beq $t0, $t1, esci
            lb $a0, ($s0)
            jal toUpper
            sb $v0, ($s0)
            addi $s0, $s0, 1
            addi $t0, $t0, 1
            j loop
esci:       li $v0, 10
            syscall
            .end main

toUpper:    addi $v0, $a0, -32
            jr $ra