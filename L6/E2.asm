            .data
            .text
            .globl main
            .ent main
main:       
            li $v0, 5
            syscall
            move $a0, $v0
            addi $sp, $sp, -4
            sw $a0, ($sp)
            jal stampaTriangolo
            li $a0, 10
            li $v0, 11
            syscall
            lw $a0, ($sp)
            addi $sp, $sp, 4
            move $a0, $t0
            jal stampaQuadrato
            li $v0, 10
            syscall
            .end main

stampaTriangolo:    add $t0, $0, $0
                    move $t2, $a0
forLoopExtT:        beq $t0, $t2, exitT
                    and $t1, $0, $0
                    addi $t0, $t0, 1
forLoopIntT:        beq $t1, $t0, exitIntT
                    addi $t1, $t1, 1
                    li $a0, 42
                    li $v0, 11
                    syscall
                    j forLoopIntT
exitIntT:           li $a0, 10
                    li $v0, 11
                    syscall
                    j forLoopExtT
exitT:              jr $ra

stampaQuadrato:     add $t0, $0, $0
                    move $t2, $a0
forLoopExtQ:        beq $t0, $t2, exitQ
                    and $t1, $0, $0
                    addi $t0, $t0, 1
forLoopIntQ:        beq $t1, $t2, exitIntQ
                    addi $t1, $t1, 1
                    li $a0, 42
                    li $v0, 11
                    syscall
                    j forLoopIntQ
exitIntQ:           li $a0, 10
                    li $v0, 11
                    syscall
                    j forLoopExtQ
exitQ:              jr $ra