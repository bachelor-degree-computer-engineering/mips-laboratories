            .data
vettore:    .word 10, 20, 1, 45, 26
DIM = 5
            .text
            .globl main
            .ent main

main:       la $a0, vettore
            li $a1, DIM
            jal massimo
            move $a0, $v0
            li $v0, 1
            syscall
            li $v0, 10
            syscall
            .end main


massimo:    and $t0, $0, $0
            lw $v0, ($a0)
            add $t0, $t0, 1
            add $a0, $a0, 4
loop:       beq $t0, $a1, exit
            lw $t1, ($a0)
            blt $t1, $v0, noMax
            move $v0, $t1
noMax:      addi $t0, $t0, 1
            add $a0, $a0, 4
            j loop
exit:       jr $ra