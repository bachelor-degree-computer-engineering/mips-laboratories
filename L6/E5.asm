            .data
            .text
            .globl main
            .ent main

main:       li $a0, 6
            li $a1, 3
            jal combina
            move $a0, $v0
            li $v0, 1
            syscall    
            li $v0, 10
            syscall
            .end main

combina:    move $s0, $a0
            move $s1, $a1
            sub $t0, $s0, $s1
            addi $t1, $0, 1
loop1:      beq $s0, $t0, exit1
            mul $t1, $t1, $s0
            addi $s0, $s0, -1
            j loop1
exit1:      move $a0, $s1
            addi $sp, $sp, -8
            sw $ra, ($sp)
            sw $t1, 4($sp)
            jal fattoriale
            lw $t1, 4($sp)
            lw $ra, ($sp)
            addi $sp, $sp, 8
            div $v0, $t1, $v0      
            jr $ra

fattoriale: addi $t0, $0, 1
            addi $t1, $0, 1
loop2:      bgt $t0, $a0, exit2
            mul $t1, $t1, $t0
            addi $t0, $t0, 1
            j loop2
exit2:      move $v0, $t1
            jr $ra