            .data
matrice:    .word 1, 0, 0
            .word 0, 5, 0
            .word 0, 0, 2
            .text
            .globl main
            .ent main

main:       addi $sp, $sp, -4
            sw $ra, ($sp)

            la $t0, matrice
            lw $a0, ($t0)
            lw $a1, 4($t0)
            lw $a2, 8($t0)
            lw $a3, 12($t0)

            lw $t1, 16($t0)
            addi $sp, $sp, -4
            sw $t1, ($sp)
            
            lw $t1, 20($t0)
            addi $sp, $sp, -4
            sw $t1, ($sp)
            
            lw $t1, 24($t0)
            addi $sp, $sp, -4
            sw $t1, ($sp)

            lw $t1, 28($t0)
            addi $sp, $sp, -4
            sw $t1, ($sp)
            
            lw $t1, 32($t0)
            addi $sp, $sp, -4
            sw $t1, ($sp)
            

            jal determinante3x3
            move $a0, $v0
            li $v0, 1
            syscall

            addi $sp, $sp, 20
            lw $ra, ($sp)
            jr $ra
            .end main

            .ent determinante3x3
determinante3x3:
            addi $sp, $sp, -4
            sw $ra, ($sp)
            move $t0, $a0
            move $t1, $a1
            move $t2, $a2
            move $t3, $a3
            lw   $t8, 4($sp)
            lw   $t7, 8($sp)
            lw   $t6, 12($sp)
            lw   $t5, 16($sp)
            lw   $t4, 20($sp)
            #salvo il $t0-8 la matrice

            addi $sp, $sp, -4
            sw $t0, ($sp)
            addi $sp, $sp, -4
            sw $t1, ($sp) 

            move $a0, $t4
            move $a1, $t5
            move $a2, $t7
            move $a3, $t8

            jal determinante2x2
            move $s0, $v0

            lw $t1, ($sp)
            addi $sp, $sp, 4
            lw $t0, ($sp)
            addi $sp, $sp, 4

            addi $sp, $sp, -4
            sw $t0, ($sp)
            addi $sp, $sp, -4
            sw $t1, ($sp) 

            move $a0, $t3
            move $a1, $t4
            move $a2, $t6
            move $a3, $t7

            jal determinante2x2
            move $s1, $v0

            lw $t1, ($sp)
            addi $sp, $sp, 4
            lw $t0, ($sp)
            addi $sp, $sp, 4

            addi $sp, $sp, -4
            sw $t0, ($sp)
            addi $sp, $sp, -4
            sw $t1, ($sp) 

            move $a0, $t4
            move $a1, $t5
            move $a2, $t7
            move $a3, $t8

            jal determinante2x2
            move $s2, $v0

            lw $t1, ($sp)
            addi $sp, $sp, 4
            lw $t0, ($sp)
            addi $sp, $sp, 4

            move $s2, $v0

            mul $s3, $t0, $s0
            mul $s4, $t1, $s1
            mul $s5, $t2, $s2

            sub $v0, $s3, $s4
            add $v0, $v0, $s5

            lw $ra, ($sp)
            addi $sp, $sp, 4
            jr $ra
            .end determinante3x3

            .ent determinante3x3
determinante2x2:   
            mul $t0, $a0, $a3
            mul $t1, $a1, $a2
            sub $v0, $t0, $t1
            jr $ra
            .end determinante2x2