            .data
matrice:    .word 1, 2, 3, 4
            .text
            .globl main
            .ent main

main:       addi $sp, $sp, -4
            sw $ra, ($sp)
            la $t0, matrice
            lw $a0, 0($t0)
            lw $a1, 4($t0)
            lw $a2, 8($t0)
            lw $a3, 12($t0)
            jal determinante2x2
            move $t0, $v0
            lw $ra, ($sp)
            addi $sp, $sp, 4
            jr $ra
            .end main

determinante2x2:   
            mul $t0, $a0, $a3
            mul $t1, $a1, $a2
            sub $v0, $t0, $t1
            jr $ra