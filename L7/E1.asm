            .data
            .text
            .globl main
            .ent main
main:       
            li $t0, 4
            li $t1, 2
            li $t2, -5
            li $t3, 3

            li $s0, 8
            li $s1, 4
            li $s2  27
            li $s3, 9
            li $s4, 64
            li $s5  16

            li $a0, 4
            li $a1, 33
            li $a2, 111
            li $a3, 271
            addi $sp, $sp, -16
            sw $t0, ($sp)
            sw $t1, 4($sp)
            sw $t2, 8($sp)
            sw $t3, 12($sp)
            addi $sp, $sp, -4
            li $t4, 6
            sw $t4, ($sp)
            jal polinomio
            move $s6, $v0
            addi $sp, $sp, 4
            lw $t3, 12($sp)
            lw $t2,  8($sp)
            lw $t1,  4($sp)
            lw $t0,   ($sp)
            addi $sp, $sp, 16
            li $v0, 10
            syscall
            .end main

polinomio:  lw $t4, ($sp)
            addi $sp, $sp, 4
            addi $sp, $sp, -24
            sw $s0, ($sp)
            sw $s1, 4($sp)
            sw $s2, 8($sp)
            sw $s3, 12($sp)
            sw $s4, 16($sp)
            sw $s5, 20($sp)
            
            sub $t0, $a1, $a0
            sub $t1, $a2, $a1
            sub $t2, $a3, $a2
            sub $s0, $t1, $t0
            sub $s1, $t2, $t1
            sub $s2, $s1, $s0
            move $v0, $a3
            and $t3, $0, $0
            addi $t4, $t4, -4
loop:       beq $t3, $t4, exit
            add $s1, $s1, $s2
            add $t2, $t2, $s1
            add $v0, $v0, $t2
            addi $t3, $t3, 1
            j loop
exit:       lw $s5, 20($sp)
            lw $s4, 16($sp)
            lw $s3, 12($sp)
            lw $s2,  8($sp)
            lw $s1,  4($sp)
            lw $s0,   ($sp)
            addi $sp, $sp, 24
            jr $ra