            .data
            .text
            .globl main
            .ent main

main:       li $a0, 19
            jal sequenzaDiCollatz
            move $a0, $v0
            li $v0, 1
            syscall
            li $v0, 10
            syscall
            .end main

sequenzaDiCollatz:
            and $t0, $0, $0
            addi $t2, $0, 1
            move $t1, $a0
            addi $sp, $sp, -4
            sw $ra, ($sp)

loop:       beq $t1, $t2, exit

            addi $sp, $sp, -12
            sw $t0, ($sp)
            sw $t1, 4($sp)
            sw $t2, 8($sp)

            move $a0, $t1

            jal calcolaSuccesivo
            
            lw $t2, 8($sp)
            lw $t1, 4($sp)
            lw $t0, ($sp)   
            addi $sp, $sp, 12
            
            move $t1, $v0
            
            addi $t0, $t0, 1
            j loop
exit:       move $v0, $t0
            lw $ra, ($sp)
            addi $sp, $sp, 4
            jr $ra



calcolaSuccesivo:
            addi $t0, $0, 1
            and $t1, $a0, $t0
            beq $t1, $0, pari
            mul $v0, $a0, 3
            add $v0, $v0, 1
            j endIf
pari:       srl $v0, $a0, 1
endIf:      jr $ra