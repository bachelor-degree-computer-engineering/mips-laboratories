            .data
            .text
            .globl main
            .ent main

main:       li $a0, 5
            jal calcolaSuccesivo
            li $v0, 10
            syscall
            .end main

calcolaSuccesivo:
            addi $t0, $0, 1
            and $t1, $a0, $t0
            beq $t1, $0, pari
            mul $v0, $a0, 3
            add $v0, $v0, 1
            j exit
pari:       srl $v0, $a0, 1
exit:       move $a0, $v0
            li $v0, 1
            syscall
            move $v0, $a0
            jr $ra