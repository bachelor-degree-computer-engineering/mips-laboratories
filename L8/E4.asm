DIM = 5
            .data 
vet1:       .word     56, 12, 98, 129, 58 
vet2:       .word     1, 0, 245, 129, 12 
risultato:  .space DIM 
            .text 
            .globl main 
            .ent main 
main:       la $a0, vet1 
            la $a1, vet2 
            la $a2, risultato 
            li $a3, DIM 
            jal CalcolaDistanzaH 
            .end main

CalcolaDistanzaH:
            addi $sp, $sp, -4
            sw $ra, ($sp)
            and $t0, $0, $0
loopExt:    beq $t0, $a3, exitLoopExt
            lw $t1, ($a0)
            lw $t2, ($a1)

            addi $sp, $sp, -4
            sw $t0, ($sp)
            addi $sp, $sp, -4
            sw $t1, ($sp)
            addi $sp, $sp, -4
            sw $t2, ($sp)
            addi $sp, $sp, -4
            sw $a0, ($sp)
            addi $sp, $sp, -4
            sw $a1, ($sp)

            move $a0, $t1
            move $a1, $t2
            jal contaDifferenze
            sw $v0, ($a2)

            lw $a1, ($sp)
            addi $sp, $sp, 4
            lw $a0, ($sp)
            addi $sp, $sp, 4
            lw $t2, ($sp)
            addi $sp, $sp, 4
            lw $t1, ($sp)
            addi $sp, $sp, 4
            lw $t0, ($sp)
            addi $sp, $sp, 4

            addi $a2, $a2, 4
            addi $a0, $a0, 4
            addi $a1, $a1, 4
            addi $t0, $t0, 1
            j loopExt
exitLoopExt: 
            lw $ra, ($sp)
            addi $sp, $sp, 4
            jr $ra

contaDifferenze:
            and $t0, $0, $0
            li $t1, 1
            and $v0, $0, $0
loop:       beq $t0, 32, exit
            and $t2, $a0, $t1
            and $t3, $a1, $t1
            beq $t2, $t3, noIf
            addi $v0, $v0, 1
noIf:       sll $t1, $t1, 1
            addi $t0, $t0, 1
            j loop
exit:       jr $ra