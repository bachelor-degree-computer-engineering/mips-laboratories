                .data
ora_in:         .byte 12, 47
ora_out:        .byte 18, 14
X:              .byte 1
Y:              .byte 40
                .text
                .globl main
                .ent main

main:           la $a0, ora_in
                la $a1, ora_out
                lbu $a2, X
                lbu $a3, Y
                jal costoParcheggio
                move $s0, $v0
                li $v0, 10
                syscall
                .end main


costoParcheggio:
                # Calcolare i minuti di permaneza
                lbu $t0, ($a1)
                lbu $t1, 1($a1)
                lbu $t2, ($a0)
                lbu $t3, 1($a0)
                mul $t0, $t0, 60
                mul $t2, $t2, 60
                add $t0, $t0, $t1
                add $t2, $t2, $t3
                #t0 contiene il minuto di uscita 
                #t2 contiene il minuto di entrata
                # $t0 -> minuti di permanenza
                sub $t0, $t0, $t2
                div $t0, $a3
                mflo $t1           # t1 contiene il quoziente
                mfhi $t2           # contiente il resto
                mul $v0, $t1, $a2
                beq $t2, $0, done
                add $v0, $v0, $a2
done:           jr $ra
                .end costoParcheggio