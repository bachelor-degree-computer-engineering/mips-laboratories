CHAR = '%'
            .data 
str_orig:   .asciiz "% nella citta' dolente, % nell'eterno dolore, % tra la perduta gente %" 
str_sost:   .asciiz "per me si va" 
str_new:    .space 200
            .text 
            .globl main 
            .ent main 
main:       la $a0, str_orig 
            la $a1, str_sost 
            la $a2, str_new 
            jal sostituisci
            li $v0, 10
            syscall
            .end main

sostituisci:
loop:       lb $t0, ($a0)
            beq $t0, 0x00, exitLoop
            bne $t0, CHAR, copyChar
            move $t9, $a1
loopInt:    lb $t0, ($a1)
            beq $t0, 0x00, end
            sb $t0, ($a2)
            addi $a2, $a2, 1
            addi $a1, $a1, 1
            j loopInt
copyChar:   sb $t0, ($a2)
end:        move $a1, $t9
            addi $a2, $a2, 1
            addi $a0, $a0, 1
            j loop
exitLoop:
            jr $ra