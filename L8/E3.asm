NUM  = 5
DIM = NUM * 4
SCONTO = 30
ARROTONDA = 1
                .data
prezzi:         .word 39, 1880, 2394, 1000, 1590
scontati:       .space DIM
                .text
                .globl main
                .ent main

main:           la $a0, prezzi 
                la $a1, scontati 
                li $a2, NUM 
                li $a3, SCONTO 
                li $t0, ARROTONDA 
                subu $sp, 4         
                sw $t0, ($sp) 
                jal calcola_sconto 
                move $s0, $v0
                li $v0, 10
                syscall
                .end main

calcola_sconto: and $t0, $0, $0
                and $v0, $0, $0
                li $t8, 100
                lw $t9, ($sp)   #$t9 contiene arrotonda
loop:           beq $t0, $a2, exitLoop
                lw $t1, ($a0)
                mul $t2, $t1, $a3
                div $t2, $t8
                mfhi $t3 
                beq $t9, $0, isLess
                blt $t3, 50, isLess
                mflo $t3
                addi $t3, $t3, 1
                add $v0, $v0, $t3
                sub $t1, $t1, $t3
                sw $t1, ($a1)
                j exitIf
isLess:         mflo $t3
                add $v0, $v0, $t3
                sub $t1, $t1, $t3
                sw $t1, ($a1)
exitIf:         addi $a0, $a0, 4
                addi $a1, $a1, 4
                addi $t0, $t0, 1
                j loop
exitLoop: 
                jr $ra