                .data
anni:           .word 1945, 2008, 1800, 2006, 1748, 1600
risultato:      .byte 0, 0, 0, 0, 0, 0
lunghezza:      .byte 6
                .text
                .globl main
                .ent main

main:           la $a0, anni
                la $a1, risultato
                lbu $a2, lunghezza
                jal bisestile
                li $v0, 10
                syscall
                .end main

bisestile:          and $t0, $0, $0
                    addi $t5, $0, 1
loop:               beq $t0, $a2, exitLoop
                    lw $t1, ($a0)
                    li $t3, 100
                    div $t1, $t3
                    mfhi $t2
                    bne $t2, $0, nonDivisibilePer100
                    li $t3, 400
                    div $t1, $t3
                    mfhi $t2
                    bne $t2, $0, nonDivisibilePer400
                    sb $t5, ($a1)
                    j exitIf
nonDivisibilePer400:sb $0, ($a1)
                    j exitIf
nonDivisibilePer100:li $t3, 4
                    div $t1, $t3
                    mfhi $t2
                    bne $t2, $0, nonDivisibilePer4
                    sb $t5, ($a1)
                    j exitIf
nonDivisibilePer4:  sb $0, ($a1)
                    j exitIf
exitIf:             addi $a0, $a0, 4
                    addi $a1, $a1, 1
                    addi $t0, $t0, 1
                    j loop
exitLoop:           jr $ra