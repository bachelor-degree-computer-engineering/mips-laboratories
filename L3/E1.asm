            .data
err_msg:    .asciiz "\nInserito carattere non numerico\n"
            .text
            .globl main
            .ent main
main:       
loop:       li $v0 12
            syscall
            move $s0 $v0
            beq $s0 '\n' exit
            bgt $s0, '9', error
            blt $s0, '0', error 
            j loop
exit:       j end
error:      la $a0 err_msg
            li $v0 4
            syscall    
end:        li $v0 10
            syscall
            .end main