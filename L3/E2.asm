            .data
err_msg:    .asciiz "\nInserito carattere non numerico\n"
msg2:       .asciiz "\noverflow\n"
            .text
            .globl main
            .ent main
main:       
            add $t0, $0, $0 # conterrà il valore
            add $t1 $0 $0   # accumulatore
            li $t3, '0'
loop:       li $v0 12
            syscall
            move $s0 $v0
            beq $s0 '\n' exit
            bgt $s0, '9', error
            blt $s0, '0', error 
            mulo $t2 $t0 10
            sub $s0 $s0 $t3
            add $t0 $t2 $s0
            blt $t0 $s0 overflow
            j loop
exit:       move $a0, $t0
            li $v0 1
            syscall
            j end
overflow:   la $a0, msg2
            li $v0 4
            syscall 
            j end
error:      la $a0 err_msg
            li $v0 4
            syscall    
end:        li $v0 10
            syscall
            .end main