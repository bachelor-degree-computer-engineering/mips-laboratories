            .data
DIM = 3
            .text
            .globl main
            .ent main
main:         
            and $t0, $0, $0
            and $t1, $0, $0
loop:       li $v0, 5
            syscall
            add $t1, $t1, $v0
            addi $t0, $t0, 1
            bne $t0, DIM, loop
            div $t1, $t1, DIM		
            move $a0, $t1
            li $v0, 1
            syscall			
            li $v0, 10
            syscall
            .end main