# Convertire in maiuscolo le prime 4 variabili
            .data
var1:       .byte 'm'
var2:       .byte 'i'
var3:       .byte 'p'
var4:       .byte 's'
var5:       .byte 0
 
            .text
            .globl main
            .ent main
main:
            lb $t0 var1
            addi $t0 $t0 -32
            sb $t0 var1

            lb $t0 var2
            addi $t0 $t0 -32
            sb $t0 var2

            lb $t0 var3
            addi $t0 $t0 -32
            sb $t0 var3

            lb $t0 var4
            addi $t0 $t0 -32
            sb $t0 var4

            la $t0 var1
            move $a0 $t0
            li $v0 4
            syscall
            # Si noti che var5 funge da terminatore di stringa
            li $v0 10
            syscall
            .end main
